from client import *

print "Enter port: "
port = input()

client = Client()
client.run('10.0.0.1', port)

# Uncomment ''' for testing interfaces
'''
addr_list = client.run('10.0.0.1', 80)
print addr_list
new_client = Client()
new_client.run(addr_list[1], 80)
'''
