import socket
import sys
import random
import time
import os
from interfaces import *
from Packet import *
from subflow import *

class Server:
	interfaces = None
	
	def __init__(self, IP, port):
		self.IP = IP
		self.port = port
		self.socket = None
		self.address = None
	
	def run(self):
	
		# Create a TCP/IP socketV
		print >> sys.stderr, 'S: Creating TCP/IP socket...'
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print >> sys.stderr, 'S: SUCCESS.'
		
		# Bind socket to port
		self.address = (self.IP, self.port)
		print >> sys.stderr, 'S: Staring up on %s port %s...' % self.address
		self.socket.bind(self.address)
		print >> sys.stderr, 'S: SUCCESS.'
		self.port = self.socket.getsockname()[1]
		print "Port: " + str(self.port)
		
		start_connection(self.socket)
		
# First TCP socket connection, also acts as the control connection for checking multipath capability
#
def start_connection(socket):
	
	# Listen for incoming connections
	socket.listen(1)
	print >> sys.stderr, 'Waiting for a connection...'
	
	while True:
		connection, client_address = socket.accept()
		print >> sys.stderr, 'S: SUCCESS.\nConnection from ', client_address
		
		try:
			is_capable = check_mulitpath_capability(connection)
			request = receive_message(connection)
			
			if not is_capable:
				send_file(connection, request)		# if has no multipath capability then use regular tcp socket to send file request
				
			else:
				addresses = get_addresses()		
				print addresses
				
				# TODO: fix the threads hahahaha kasi I use time.sleep for now para patapusin muna ung socket connections
				#       before getting port numbers to send address + port to client 
				# TODO: baka make diff function for multipath 
				subflow_list = create_subflows(addresses)			
				time.sleep(5)
				send_available_addresses(connection, subflow_list)
				time.sleep(5)
				send_using_multiple_paths(subflow_list)
				
		finally:
			connection.close()
			
# Receives a message from client, checks if this message indicates multipath capability: Returns True if yes, otherwise False. 
# TODO: This is bare bones checking of mp_capability. Might need to be more thorough with usage of tokens like in PLMT.
#				
def check_multipath_capability(connection):
	
	message = receive_message(connection)
	
	#check, token = message.split(":")
	check = message
	
	if check == "Multipath_capable":
		socket.sendall(check)
		return True
	else:
		return False

# Accepts a socket connection as parameter and receives message from client. Returns the message to function caller.
#			
def receive_message(connection):
	
	message = ""
	while True:
		data =  connection.recv(1024)
		if data:
			message = message + data
		else:
			break
	
	return message
	
# Test function. Opens a (video) file and sends it to the client.
#
def send_file(connection, filename):
	
	file_path = "/home/corubio/Desktop/THESIS/" + filename
	print os.path.exists(file_path)
	
	bytes = open(file_path).read()
	
	connection.sendall(bytes)
	connection.close()

# Opens a video file as bytes: Returns video file in bytes. TODO: Make this general for all file requests.
#
def get_video_data():
	
	videofile = "/home/corubio/Desktop/THESIS/Wildlife_480p.mp4"
	bytes = open(videofile).read()
	
	return bytes

# Sends available addresses to client || hiniwalay ko ung pagkuha ng IP addresses and put it sa interfaces.py 
# TODO: pag-usapan pa natin ung delimiters i guess.
def send_available_addresses(connection, subflow_list):
	
	message = ""
	
	for subflow in subflow_list:
		message = message + subflow.get_IP() + ":" + str(subflow.get_port()) + " "
		
	print >> sys.stderr, '%s', message
	
	print >> sys.stderr, 'S: Sending packet...'
	connection.sendall(message)
	connection.close()

# Makes use of the available paths/subflows to send data chunks to the client
# TODO: fix the threading, currently makes use of time.sleep(1) para di magkagulo ung pagsend
#       also ung pagpili din ng subflow. code works for only two paths rn	
def send_using_multiple_paths(subflow_list):

	#data = "Cats have 32 muscles in their ears (humans have only 12). This gives the ear mobility, enabling it to precisely locate prey such as mice or the opening of their cat food! Cats can also hear frequencies that are both below and above those that can be heard by humans. The ear also has the job of helping to maintain balance and the ability to right themselves when falling - which is where the phrase Cats always land on their feet came from."
	
	#print data
	
	data = get_video_data()
	
	packet_list = split_into_packets(data)
	
	print "number of packets " + str(len(packet_list))
	random.shuffle(packet_list)
	
	count = 1
	for packet in packet_list:
		if count%2 == 0:
			subflow_list[1].send_data(packet.get_packet())
		else:
			subflow_list[0].send_data(packet.get_packet())	
		count = count + 1
		time.sleep(1)	# TODO: remove
		
	subflow_list[0].send_data("end")
	subflow_list[1].send_data("end")

	
