import time
from server import *
from subflow import *

server = Server('10.0.0.1', 0)
server.run()

# Uncomment ''' for testing subflow thread creation
'''
address_list = ['10.0.0.1', '10.0.1.1']
subflows = []

for a in address_list:
	subflows.append(create_subflows(a))
		
time.sleep(5)
	
for s in subflows:
	print s.get_address()
	s.close()
'''

# Uncomment ''' for testing interfaces
'''
addr_list = server.run()
print addr_list
new_server = Server(addr_list[1], 80)
new_server.run()
'''
