import socket
import sys
import time
import subprocess
import os
from Packet import *
from client_subflow import *

class Client:
	def __init__(self):
		self.socket = None
	
	def run(self, IP, port):
		# Create a TCP/IP socket
		print >> sys.stderr, 'C: Creating TCP/IP socket...'
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print >> sys.stderr, 'C: SUCCESS.'
		
		# Connect the socket to the server
		server_address = (IP, port)
		print >> sys.stderr, 'Connectiong to %s port %s...' % server_address
		self.socket.connect(server_address)
		print >> sys.stderr, 'SUCCESS.'

		print >> sys.stderr, 'Receiving packets...'
		try:
			print "client"
			
			is_capable = check_multipath_capability(self.socket)
			
			# TODO: add file request message sent to server, accept function to be called will depend on server multipath capability			
			
			
			
			#receive_packets(self.socket)
			#receive_videofile(self.socket)		# Uncomment to check sending video file from server to client through one TCP socket
			
			'''
			addresses = receive_addresses(self.socket)
			subflow_list = create_subflows(addresses)
			
			# TODO: Untested. Added in place of a time.sleep(num) waiting for server to send all packets to client before moving on to next parts
			while True:
				subflow_num = len(subflow_list)
				done = 0
				for subflow in subflow_list:
					if subflow.get_received_packets():
						done = done + 1
				if done >= subflow_num:
					print done
					break					
			
			data = reorder_packets(subflow_list)
			play_videofile(data)
			'''
		finally:
			self.socket.close()
			print "done"

# Checks server multipath capability, Returns True if yes, otherwise False
#
def check_multipath_capability(socket):
	
	socket.sendall("Multipath_capable")
	message = socket.recv(2024)
	
	if message == "Multipath_capable":
		return True	
	else:
		return False

# Creates a subprocess that calls VLC player and plays the data parameter
#
def play_videofile(data):
	
	cmdline = ['vlc-wrapper', '-']
	player = subprocess.Popen(cmdline, stdin=subprocess.PIPE)
	player.stdin.write(data)

# Test Function. Creates a subprocess that calls VLC player to play received data from server.
# Can either wait for entire data block to be recived or write in data to VLC to play video in real time.
#	
def receive_videofile(socket):
	
	cmdline = ['vlc-wrapper', '-']
	player = subprocess.Popen(cmdline, stdin=subprocess.PIPE)
	video = ""
	while True:
		data = socket.recv(1024)
		video = video + str(data)
		if not data:
			break
		#player.stdin.write(data)		# isaksak kay vlc to play ano matanggap na data (TCP is in order delivery)
	player.stdin.write(video)			# wait for all data to arrive before playing in VLC 
	
	socket.close()
	
# Receives data chunk from server containing available addresses, parses to get the addresses, and returns address list 
#
def receive_addresses(socket):
	message = ""
	while True:
		data = socket.recv(1024)
		if data:
			message = message + data
			socket.sendall("ok")
		else:
			break
	print >> sys.stderr, 'SUCCESS. Available addresses received: ' + message
	socket.close()

	addresses = message.strip(' ').split(' ')
	
	return addresses	

# Reorders the packets received from the multiple paths/subflows into one list
# 
def reorder_packets(subflow_list):
	
	packets = []
	
	count = 0
	for subflow in subflow_list:
		packet_list = subflow.get_received_packets()
		for packet in packet_list:
			count = count + 1
			packets.append(packet)
			
	app_buffer = sorted(packets, key=lambda x: x.get_stream_offset(), reverse=False)
	print "\n"
	print "Received packets " + str(count)
	return get_data_block_from_chunks(app_buffer)

# Recontructs message from out of order data chunks received from the server
#			
def get_data_block_from_chunks(packet_list):
		
	data_block = ""
	for p in packet_list:
		data_block = data_block + p.get_data_chunk()
			
	return data_block



