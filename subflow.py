import threading
import sys
import socket

# Threaded class that server uses to create "subflows" when new TCP/IP sockets are created
#
class Subflow(threading.Thread):
	
	def __init__(self, IP):
		threading.Thread.__init__(self)
		self.__IP = IP
		self.__port = None
		self.__socket = None
		self.__address = None
		self.__connection = None
	
	def run(self):
		# Create a TCP/IP socket
		print >> sys.stderr, 'S: Creating TCP/IP socket...'
		self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print >> sys.stderr, 'S: SUCCESS.'
	
		# Bind socket to port
		self.__address = (self.__IP, 0)
		print >> sys.stderr, 'S: Staring up on %s port %s...' % self.__address
		self.__socket.bind(self.__address)
		print >> sys.stderr, 'S: SUCCESS.'
		self.__port = self.__socket.getsockname()[1]	
		self.__address = (self.__IP, self.__port)
		
		# Listen for incoming connections
		self.__socket.listen(1)
		print >> sys.stderr, 'S: Listening for connections...'
		
		self.__connection, client_address = self.__socket.accept()
	
	def get_IP(self):
		return self.__IP
		
	def get_port(self):
		return self.__port
		
	def get_socket(self):
		return self.__socket
		
	def get_address(self):
		return self.__address
		
	def send_data(self, data):
		#print >> sys.stderr, 'S: Sending packet using ' + self.__IP
		self.__connection.sendall(data)
		while True:
			ack = self.__connection.recv(16)
			if ack:
				break
			
	'''	
	def close(self):
		self.__socket.close()
		print "end"
	'''	
	
# Creates instances of Subflow class and starts these threads (creates Sockets litening for connections)
#	
def create_subflows(addresses):
	
	subflow_thread_list = []
	
	for address in addresses:
		subflow_thread = Subflow(address)
		subflow_thread.start()
		subflow_thread_list.append(subflow_thread)
	
	return subflow_thread_list
	
