from itertools import groupby

# A data packet
# chunk size - (32 bits) informs receiver of chunk payload size, i.e.
#              how many bytes of data will follow the header
# stream offset - (64 bits) indicates position of chunk within the
#              stream, used to copy the chunk in the right position of
#              application buffer || for now, use as chunk index
# block size - (32 bits) allows receiver to check if the block can fit in
#              the app buffer, if too large preallocate addtnl temp buffer
# block index - (32 bits) used to verify that chunk belongs to the block
#			   currently being received
# data in bytes? 
#
class Packet:
	def __init__(self, chunk_size, stream_offset, block_size, block_index, data_chunk):
		self.__chunk_size = chunk_size
		self.__stream_offset = stream_offset
		self.__block_size = block_size
		self.__block_index = block_index
		self.__data_chunk = data_chunk
	
	def get_chunk_size(self):
		return self.__chunk_size
	
	def get_stream_offset(self):
		return self.__stream_offset
		
	def get_block_size(self):
		return self.__block_size
		
	def get_block_index(self):
		return self.__block_index
		
	def get_data_chunk(self):
		return self.__data_chunk	
	
	# Returns a string (packet) with the header info in bits + data chunk for sending string packet through socket stream
	#
	def get_packet(self):
		chunk_size_in_bits = convert_int_to_binary(self.__chunk_size, 32)
		stream_offset_in_bits = convert_int_to_binary(self.__stream_offset, 64)
		block_size_in_bits = convert_int_to_binary(self.__block_size, 32)
		block_index_in_bits = convert_int_to_binary(self.__block_index, 32)
		packet = chunk_size_in_bits + stream_offset_in_bits + block_size_in_bits + block_index_in_bits + self.__data_chunk
		return packet

# Splits a data block into chunks and adds header information, returns a list of packets
#		
def split_into_packets(data):
	chunk_size = 700		# TODO: data chunk size for each packet
	block_size = len(data)
	block_index = 0			# TODO: nothing set for now
	stream_offset = 0
	#chunk_list = [data[i:i+64] for i in range(0, block_size, 64)]
	chunk_list = [data[i:i+chunk_size] for i in range(0, block_size, chunk_size)] 
	
	packet_list = []
	for c in chunk_list:
		chunk_size = len(c)
		packet_list.append(Packet(chunk_size, stream_offset, block_size, block_index, c))	
		
		stream_offset = stream_offset + chunk_size
	
	return packet_list

# Accepts data chunk, parses the control information from the data, and converts it into a Packet instance: Returns Packet instance
#	
def convert_to_packet(data):
	chunk_size = int(data[0:32], 2)  	# 32 bits
	stream_offset = int(data[32:96], 2) # 64 bits
	block_size = int(data[96:128], 2)   # 32 bits
	block_index = int(data[128:160], 2) # 32 bits
	data_chunk = data[160:len(data)] 	# remainder
	packet = Packet(chunk_size, stream_offset, block_size, block_index, data_chunk)
	
	return packet 

# Adds data chunk into application buffer || Currently unused ||
#
#def add_to_buffer(pipe, app_buffer, chunk_offset, chunk_size):
def add_to_buffer(packet, app_buffer):
	#TODO fix this part. 
	app_buffer.insert(packet.get_stream_offset(), packet.get_data_chunk())
	
	return app_buffer
	
# Converts int to x bit binary, either 32 or 64 bits (for use in control information)
#
def convert_int_to_binary(num, bits):
	if bits == 32:
		return '{0:032b}'.format(num)
	elif bits == 64:
		return '{0:064b}'.format(num) 	

# Helper function that prints out attributes of a Packet instance
#
def print_packet_attributes(packet):
	print 'chunk_size = ' + str(packet.get_chunk_size())
	print 'stream_offset = ' + str(packet.get_stream_offset())
	print 'block_size = ' + str(packet.get_block_size())
	print 'block_index = ' + str(packet.get_block_index())
	print 'data_chunk = ' + str(packet.get_data_chunk())


### TEST all functions### comment out when done using ''' before and after block of code
'''
import random

print 'Creating a random 64 byte block of data for testing'
bytes = random._urandom(100)
print 'DONE.\n'

print 'Testing split_into_packets: returns list of instances of class Packet'
packet_list = split_into_packets(bytes)
print len(packet_list)
print 'DONE.\n'

print 'Print out attributes of every instance of Packet'
for p in packet_list:
	print_packet_attributes(p)
	print '\n'
print 'DONE\n'

print 'Testing get_packet: prints Packet instance attributes as one packet block'
for p in packet_list:
	packet = p.get_packet()
	print packet
	print '\n'
print 'DONE.\n'

print 'Testing convert_to_packet: returns a Packet instance for the received packet chunk'
packet = convert_to_packet(packet_list[0].get_packet())
print_packet_attributes(packet)
print 'DONE.\n'

print 'Testing add_to_buffer, returns application buffer containing in order data chunks'
app_buffer = []
app_buffer = add_to_buffer(packet, app_buffer)
print ''.join(app_buffer)
print 'DONE.\n'

print 'Testing convert_int_to_binary, returns a binary string'
bits = convert_int_to_binary(64, 32)
print bits
print int(bits, 2)
print 'DONE.\n'
'''

