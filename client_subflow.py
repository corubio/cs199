import threading
import sys
import socket
from Packet import *

class Client_Subflow(threading.Thread):
	
	def __init__(self, IP, port):
		threading.Thread.__init__(self)
		self.__IP = IP
		self.__port = port
		self.__socket = None
		self.__received_packets = []
		
	def run(self):
		# Create a TCP/IP socket
		print >> sys.stderr, 'C: Creating TCP/IP socket...'
		self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print >> sys.stderr, 'C: SUCCESS.'
		
		# Connect the socket to the server
		server_address = (self.__IP, self.__port)
		print >> sys.stderr, 'Connecting to %s port %s...' % server_address
		self.__socket.connect(server_address)
		print >> sys.stderr, 'SUCCESS.'	
		
		# Wait for packets
		self.__received_packets = receive_packets(self.__IP, self.__socket)
		
	def get_IP(self):
		return self.__IP
		
	def get_port(self):
		return self.__port
		
	def get_socket(self):
		return self.__socket
		
	def get_received_packets(self):
		return self.__received_packets

# Receives packets sent by server and converts them to a Packet instance
#		
def receive_packets(IP, socket):
	
	packets = []
	
	print IP + " waiting for packets..."
		
	while True:
		data = socket.recv(1024)
		if data == "end":
			break
			print "break"
			
		elif data:
			#print IP
			#print data
			packets.append(convert_to_packet(data))
	
	return packets
	
# Creates instances of Client_Subflow class which creates and connects sockets to server: Returns subflow list
# 
def create_subflows(addresses):

	subflow_list = []
		
	for address in addresses: 
	
		address_port = address.split(":")
		IP = address_port[0]
		port = int(address_port[1])
		
		subflow = Client_Subflow(IP, port)
		subflow.start()
		subflow_list.append(subflow)
		
	return subflow_list
